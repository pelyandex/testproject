export default function({ $axios, redirect, store }) {
  $axios.interceptors.request.use(req => {
    if (
      store.getters['auth/isAuthenticated'] &&
      !req.headers.common.Authorization
    ) {
      const token = store.getters['auth/token']
      req.headers.common.Authorization = `Bearer ${token}`
    }
    return req
  })

  $axios.onError(error => {
    if (error.response) {
      if (error.response.status === 401) {
        store.dispatch('auth/logout')
        redirect('/admin/login?message=session')
      }
      if (error.response.status === 500) {
        console.log('Server 500 error')
      }
    }
  })
}

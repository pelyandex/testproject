const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
const cors = require('cors')

const commentRoutes = require('./routes/comment.routes')
const passportStrategy = require('./middleware/passport-strategy')
const postRoutes = require('./routes/post.routes')
const app = express()
const authRoutes = require('./routes/auth.routes')
const keys = require('./keys')

mongoose
  .connect(keys.MONGO_URL, { useFindAndModify: false })
  .then(() => {
    console.log('mongoDB connected')
  })
  .catch(err => console.error(err))

app.use(cors())
app.use(passport.initialize())
passport.use(passportStrategy)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api/auth', authRoutes)
app.use('/api/post', postRoutes)
app.use('/api/comment', commentRoutes)

module.exports = app

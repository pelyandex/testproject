const Comment = require('../models/comment.model')
const Post = require('../models/post.model')

module.exports.create = async (req, res) => {
  try {
    await Comment.create({
      name: req.body.name,
      text: req.body.text,
      postId: req.body.postId
    }).then(async rez => {
      const post = await Post.findById(req.body.postId)
      post.comments.push(rez._id)
      await post.save()
      res.status(201).json(post)
    })
    // const comment = new Comment({

    // })
    // comment.save()
    // const post = await Post.findById(postId)
    // console.log(post)
    // post.comments.push(comment._id)
    // await post.save()

    // res.status(201).json(comment)
  } catch (err) {
    res.status(500).send(err)
  }
}

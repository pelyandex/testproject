const posts = [
  {
    title: 'post1',
    date: new Date(),
    views: 22,
    comments: [1, 2],
    _id: '1221'
  },
  {
    title: 'post2',
    date: new Date(),
    views: 22,
    comments: [1, 2],
    _id: '1221'
  }
]
export const actions = {
  async fetchAdmin({ commit }) {
    try {
      return await this.$axios.$get('api/post/admin/')
    } catch (err) {
      console.log(err)
    }
  },
  async fetch({ commit }) {
    try {
      return await this.$axios.$get('api/post')
    } catch (err) {
      console.log(err)
    }
  },
  async addView({ commit }, id) {
    try {
      return await this.$axios.$put(`api/post/${id}`)
    } catch (err) {
      console.log(err)
    }
  },
  async fetchById({}, id) {
    try {
      return await this.$axios.$get(`api/post/${id}`)
    } catch (err) {
      console.log(err)
    }
  },
  async fetchAdminById({}, id) {
    try {
      return await this.$axios.$get(`api/post/admin/${id}`)
    } catch (err) {
      console.log(err)
    }
  },
  async remove({ commit }, id) {
    try {
      return await this.$axios.$delete(`api/post/admin/${id}`)
    } catch (err) {
      console.log(err)
    }
  },
  async update({}, data) {
    try {
      return await this.$axios.$put(`api/post/admin/${data.id}`, {
        text: data.text
      })
    } catch (err) {
      console.log(err)
    }
  },
  async create({ commit }, { title, text, image }) {
    try {
      const fd = new FormData()
      fd.append('title', title)
      fd.append('text', text)
      fd.append('image', image, image.name)
      return await this.$axios.$post('api/post/admin', fd)
    } catch (err) {
      commit('SetError', err, { root: true })
      throw err
    }
  }
}

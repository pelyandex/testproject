import cookie from 'cookie'
import cookies from 'js-cookie'
import JwtDecode from 'jwt-decode'

export const state = () => ({
  token: null
})
export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  logout(state) {
    state.token = null
  }
}
export const actions = {
  async login({ commit, dispatch }, formData) {
    try {
      const { token } = await this.$axios.$post(
        '/api/auth/admin/login',
        formData
      )
      this.$axios.setToken(token, 'Bearer')
      dispatch('setToken', token)
    } catch (err) {
      commit('setError', err, { root: true })
      throw err
    }
  },
  async createUser({ commit }, formData) {
    try {
      await this.$axios.$post('api/auth/admin/create', formData)
    } catch (err) {
      commit('setError', err, { root: true })
      throw err
    }
  },
  async setToken({ commit }, token) {
    cookies.set('jwt-token', token)
    commit('setToken', token)
  },
  logout({ commit }) {
    this.$axios.setToken(false)
    cookies.remove('jwt-token', token)
    commit('logout')
  },
  async autoLogin({ dispatch }) {
    const cookieStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie
    const Cookie = cookie.parse(cookieStr || '') || {}
    const token = Cookie['jwt-token']
    if (isJwtValid(token)) {
      dispatch('setToken', token)
    } else {
      dispatch('logout')
    }
  }
}
export const getters = {
  isAuthenticated: state => Boolean(state.token),
  token: state => state.token
}

function isJwtValid(token) {
  if (!token) return false
  const jwtData = JwtDecode(token)
  return jwtData
}

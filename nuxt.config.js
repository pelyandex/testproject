module.exports = {
  mode: 'universal',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: { color: '#fff' },
  css: ['element-ui/lib/theme-chalk/index.css', '@/theme/index.scss'],
  plugins: ['@/plugins/globals', '@/plugins/axios'],
  buildModules: [],
  modules: ['@nuxtjs/axios', '@nuxtjs/pwa', '@nuxtjs/sitemap'],
  // pwa: {
  //   workbox: {
  //     /* workbox options */
  //   },
  //   meta: {
  //     /* meta options */
  //   }
  // },
  env: {
    appName: 'SsR blog'
  },
  server:{
    host: process.env.HOST || 'localhost'
  },
  axios: {
    baseURL: process.env.BASE_URL || 'http://localhost:3000'
  },
  build: {
    transpile: [/^element-ui/],
    extend(config, ctx) {}
  }
}
